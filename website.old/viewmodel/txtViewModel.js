function Catagory(id, display) {
    var self = this;
    self.id = id;
    self.display = ko.observable(display);
    self.snippets = ko.observableArray();

    self.numberOfSnippets = ko.computed(function() {
        return self.snippets().length;
    }, this);
}

function Snippet(catagoryId, display) {
    var self = this;
    self.catagoryId = catagoryId;
    self.display = ko.observable(display);
}

function TxtViewModel() {

    var self = this;

    self.codeVersion = ko.observable(146);
    self.catagoryDataSource = new JsonDataSource('data/catagory-json.txt');
    self.snippetDataSource = new JsonDataSource('data/snippets-json.txt');

    self.selectedSnippets = new SelectedSnippetStorage();

    self.catagories = ko.observableArray();

    self.currentText = ko.computed(function() {
        if (!self.catagoryDataSource.loadComplete()) {
            return "still loading";
        }

        if (!self.snippetDataSource.loadComplete()) {
            return "still loading";
        }

        var text = '';
        for (var snippetIndex = 0; snippetIndex < self.selectedSnippets.store.persistentArray().length; snippetIndex++) {
            text = text + self.selectedSnippets.store.persistentArray()[snippetIndex].display();
            if (snippetIndex != 0) {
                text = text + ' ';
            }
        }
        return text;
    }, this);

    self.status = ko.computed(function() {
        if (!self.catagoryDataSource.loadComplete()) {
            return "loading catagories";
        }

        if (!self.snippetDataSource.loadComplete()) {
            return "loading snippets";
        }

        var snippetCount = 0;
        for (var catagoryIndex = 0; catagoryIndex < self.catagories().length; catagoryIndex++) {
            snippetCount = snippetCount + self.catagories()[catagoryIndex].snippets().length;
        }
        return self.catagories().length + " catagories " + snippetCount + " snippets loaded";
    }, this);

    self.selectionStatus = ko.computed(function() {
        var selectionLength = self.selectedSnippets.store.persistentArray().length;

        if (selectionLength < 1) {
            return "nothing has been selected";
        }

        return selectionLength + " snippets selected";
    }, this);

    self.loadCatagories = function (catagories, fromCache, reloadFromServer) {
        for (var i = 0; i < catagories.length; i++) {
            self.catagories.push(new Catagory(catagories[i].CatagoryId,catagories[i].Display));
        }
        self.snippetDataSource.getData(reloadFromServer, self.loadSnippets);
        return i;
    };

    self.loadSnippets = function (snippets, fromCache, reloadFromServer) {
        var lodadedCount = 0;
        for (var i = 0; i < snippets.length; i++) {
            var snippet = new Snippet(snippets[i].CatagoryId,snippets[i].Display);
            for (var catagoryIndex = 0; catagoryIndex < self.catagories().length; catagoryIndex++) {
                if (self.catagories()[catagoryIndex].id == snippet.catagoryId) {
                    self.catagories()[catagoryIndex].snippets.push(snippet);
                    lodadedCount = lodadedCount + 1;
                }
            }
        }
        self.snippetsUpdated();
        return lodadedCount;
    };

    self.snippetsUpdated = function () {
        // need to refresh the UI after the data has been bound
        var list = $('.catagorylist');
        self.refreshListview(list)

        // for some reason text binding to the bubble counter does not work
        var catagoryBubbles = $('.catagorybubble');
        catagoryBubbles.each(function (index) {
            var snippetCount = self.catagories()[index].numberOfSnippets();
            $(this).text(snippetCount);
        })
    };

    self.selectedSnippetsUpdated = function () {
        // need to refresh the UI after the data has been bound
        var list = $('.textlist');
        self.refreshListview(list)
    };

    self.refreshListview = function(listview) {
        if (listview) {
            try {
                $(listview).listview('refresh');
            } catch (e) {
                try { $(listview).listview(); } catch (e) { };
            }
        }
        if ($.mobile.activePage) {
            $.mobile.activePage.trigger('pagecreate');
        }
    }

    self.addText = function (snippet, event) {
        var selectedSnippet = new SelectedSnippet(snippet.display());

        self.selectedSnippets.store.addItem(selectedSnippet);
        // may be able to do without this update as list is not visible, but safer to leave it in
        self.selectedSnippetsUpdated();
        alert('Added');
    };

    self.removeText = function (selectedSnippet, event) {
        //alert('Remove: ' + selectedSnippet.display());
        self.selectedSnippets.store.removeItem(selectedSnippet);
        self.selectedSnippetsUpdated();
    };

    self.moveUp = function (selectedSnippet, event) {
        alert('move: ' + selectedSnippet.display());
    };

    self.snippetMenuPostProcess =  function(element,selectedSnippet){
           var p = $(element).find('section').eq(0).popup();
           p.attr('id','selectedSnippetMenu'+self.selectedSnippets.store.persistentArray.indexOf(selectedSnippet) );
           ko.applyBindings(selectedSnippet,p[0]); 
           return true;
    };

    self.selectAll =  function(textId) {
        $(textId).focus(function() { 
            // need this for iOS
            this.setSelectionRange(0, 9999);
        });

        $(textId).mouseup(function(e){
            e.preventDefault();
        });

        //$(textId).focus(function (event) {
        //    this.selectionStart=0;
        //    this.selectionEnd=this.value.length;
        //});
        // $(textId).bind('vmouseup', function () {
        //     this.selectionStart=0;
        //     this.selectionEnd=this.value.length;
        // });
    };

    // code
    $( '#home' ).live( 'pageshow',function(event, ui){
        //alert( 'This page was just hidden: '+ ui.prevPage);
        // refresh the UI before the page is displsyed
        self.snippetsUpdated();
    });

    $( '#message' ).live( 'pageshow',function(event, ui){
        //alert( 'This page was just hidden: '+ ui.prevPage);
        // refresh the UI before the page is displsyed
        self.selectedSnippetsUpdated();
    });

    $( '#currenttext' ).live( 'pageshow',function(event, ui){
        //alert( 'This page was just hidden: '+ ui.prevPage);
        // refresh the UI before the page is displsyed
        self.selectAll('#clip');
    });

    self.catagoryDataSource.getData(false, self.loadCatagories);
}

ko.applyBindings(new TxtViewModel());
